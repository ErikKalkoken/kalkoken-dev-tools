# Kalkoken dev tools

Tools for supporting development of AA community apps.

Features:

- Clear celery once locks
- Generate markdown for app settings
- Drop test databases

To see an overview of available commands and syntax run the following command:

```sh
kalkoken-tools -h
```

from redis import Redis


def main(args):
    """Clear all celery once locks (if any exist)."""
    r = Redis.from_url(args.redis_url)
    if keys := r.keys(f":?:qo_{args.app_label}.*"):
        deleted_count = r.delete(*keys)
    else:
        deleted_count = 0

    print(f"Removed {deleted_count} celery once keys for {args.app_label}.")

"""Drop all test databases for the current AA installation."""
import argparse
import os
import sys


def main(args: argparse.Namespace):
    sys.path.insert(0, str(args.settings_path))

    import django

    # init and setup django project
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myauth.settings.local")
    print("AA starting up...")
    django.setup()

    """MAIN"""
    from django.db import connection

    db_name = connection.settings_dict["NAME"]

    with connection.cursor() as cursor:
        cursor.execute(
            f"""
            SELECT schema_name
            FROM information_schema.schemata
            WHERE schema_name LIKE 'test_{db_name}%';
            """
        )
        rows = cursor.fetchall()

        if not rows:
            print("No test databases found")
            sys.exit()

        database_names = sorted([row[0] for row in rows])
        print("Found these test databases:")
        for name in database_names:
            print(f"  {name}")

        result = input("Are you sure you want to drop these databases (y/N)?")
        if result != "y":
            print("Aborted")
            sys.exit()

        for name in database_names:
            print(f"Dropping database {name}")
            cursor.execute(f"DROP DATABASE {name};")

    print("DONE")

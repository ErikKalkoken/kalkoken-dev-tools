"""Main module for Kalkoken dev tools."""

import argparse
import logging
from enum import Enum
from pathlib import Path

from . import clear_celery_once, drop_test_databases, generate_settings

logger = logging.getLogger(__name__)


class Commands(str, Enum):
    GENERATE_SETTINGS = "generate-settings"
    CLEAR_CELERY_ONCE = "clear-celery-once"
    DROP_TEST_DATABASES = "drop-test-databases"


def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--log-level", choices=["DEBUG", "INFO", "WARNING", "ERROR"], default="WARNING"
    )
    subparsers = parser.add_subparsers(dest="command")

    # generate settings
    parser_settings = subparsers.add_parser(
        Commands.GENERATE_SETTINGS.value, help="Generate markdown from app settings"
    )
    parser_settings.add_argument(
        "settings_file", help="Path to app_settings.py including file name"
    )
    parser_settings.add_argument(
        "output_dir",
        nargs="?",
        help="Directory to create output file in.",
        default=Path.cwd(),
    )

    # clear celery once
    parser_celery_once = subparsers.add_parser(
        Commands.CLEAR_CELERY_ONCE.value, help="Clear celery once locks"
    )
    parser_celery_once.add_argument("app_label", help="Django app label")
    parser_celery_once.add_argument(
        "--redis-url", default="redis://127.0.0.1:6379/1", help="REDIS URL"
    )

    # drop test databases
    parse_drop_databases = subparsers.add_parser(
        Commands.DROP_TEST_DATABASES.value, help="Drop test databases"
    )
    myauth_dir = Path(__file__).parent.parent.parent.parent / "myauth"
    parse_drop_databases.add_argument(
        "--settings-path",
        default=myauth_dir,
        help="Path to where the local.py file is located",
    )

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    logger.setLevel(args.log_level)

    match args.command:
        case Commands.GENERATE_SETTINGS:
            generate_settings.main(args)

        case Commands.CLEAR_CELERY_ONCE:
            clear_celery_once.main(args)

        case Commands.DROP_TEST_DATABASES:
            drop_test_databases.main(args)

        case _:
            raise NotImplementedError(f"Command {args.command}")


if __name__ == "__main__":
    main()

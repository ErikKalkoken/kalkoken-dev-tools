"""Generate markdown for settings table from app_settings.py."""

import ast
import logging
from pathlib import Path

logger = logging.getLogger(__name__)

PRIVATE_KEYWORD = ":private:"  # ignore settings with this keyword in docstring
OUTPUT_FILENAME = "README_settings.tmp"


def fetch_settings(filepath: Path) -> dict:
    """Fetch settings incl. docstrings and defaults from an app_settings.py file."""
    with filepath.open("r") as f:
        module_text = f.read()

    result = {}
    constants = {}
    key = None
    for node in ast.walk(ast.parse(module_text)):
        logger.debug(f"Processing node: {node}")
        try:
            match node:
                case ast.Assign():
                    match node.value:
                        case ast.Call():
                            if not node.value.func.id == "clean_setting":
                                continue

                            key = node.targets[0].id
                            logger.debug(f"Found setting: {key}")
                            call = node.value
                            default_value = None
                            if len(node.value.args) > 1:
                                my_arg = call.args[1]
                                match my_arg:
                                    case ast.Constant():
                                        default_value = my_arg.value
                                    case ast.Call():
                                        default_value = my_arg.args[0].value
                                    case ast.Dict():
                                        default_value = dict(
                                            zip(my_arg.keys, my_arg.values)
                                        )
                                    case ast.List():
                                        default_value = []
                                        for e in my_arg.elts:
                                            v = e.value
                                            default_value.append(v)
                                    case _:
                                        raise RuntimeError("Can not parse this")

                            elif call.keywords:
                                for keyword in call.keywords:
                                    if keyword.arg == "default_value":
                                        match keyword.value:
                                            case ast.List():
                                                default_value = keyword.value.elts
                                            case ast.Name():
                                                id = keyword.value.id
                                                default_value = constants.get(id)
                                            case _:
                                                default_value = keyword.value.value
                                        break
                                    else:
                                        key = None
                                        continue
                            else:
                                key = None
                                continue

                            result[key] = {"default": default_value, "docstring": ""}
                            continue

                        case ast.Constant():
                            n = node.targets[0].id
                            constants[n] = node.value.s

                case ast.Expr():
                    if key:
                        value = node.value.s.replace("\n", " ").strip()
                        result[key]["docstring"] += value

        except Exception as ex:
            lines = module_text.splitlines()
            ref_line = lines[node.lineno]
            error = f"{ex.__class__.__name__}: {ex}"
            print(f"Skipping line with error: {error}\n{ref_line}\n")

        key = None

    result = {
        key: data
        for key, data in result.items()
        if PRIVATE_KEYWORD not in data["docstring"]
    }
    result = dict(sorted(result.items()))
    return result


def make_md_table(settings: dict) -> list:
    """Render markdown table from settings."""
    output = ["Name|Description|Default", "--|--|--"]

    for name, obj in settings.items():
        line = f'`{name}`|{obj["docstring"]}|`{obj["default"]}`'
        output.append(line)
    return output


def write_md_file(output_dir: str, output: list):
    """Write markdown to file."""
    output_path = Path(output_dir) / OUTPUT_FILENAME
    with output_path.open("w") as fp:
        for line in output:
            fp.write(line + "\n")

    print(f"Written settings markup to file: {output_path.absolute()}")


def main(args):
    settings_filepath = Path(args.settings_file)
    if not settings_filepath.exists():
        print(f"File does not exist: {settings_filepath}. Aborting.")
        exit(1)

    print(f"Parsing: {settings_filepath.absolute()}")
    settings = fetch_settings(settings_filepath)
    if not settings:
        print("WARNING: Did not find any settings. Aborting")
        exit(1)

    output = ["# Settings", ""]
    output += make_md_table(settings)
    write_md_file(args.output_dir, output)
